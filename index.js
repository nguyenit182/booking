import dotenv from 'dotenv';
dotenv.config();
import express from 'express';
import { connect } from './connection.js';
import morgan from 'morgan';
import authRouter from './routes/auth.js';
import usersRouter from './routes/users.js';
import roomsRouter from './routes/rooms.js';
import hotelsRouter from './routes/hotels.js';
connect();
const app = express();
app.use(morgan('common'));

app.get('/', (req, res) => {
  res.send('first request');
});

// middlewares

app.use(express.json());
app.use('/api/auth', authRouter);
app.use('/api/users', usersRouter);
app.use('/api/hotels', hotelsRouter);
app.use('/api/rooms', roomsRouter);

const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {
  console.log(`Server listening on port : ${PORT}`);
});
